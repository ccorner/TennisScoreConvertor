import java.io.*;

public class LoadFile {
  private File inputFile;


  public String loadFile(String inputFileName) {

    StringBuilder inputString = new StringBuilder();

    try{
      inputFile = new File(inputFileName);
      FileReader reader = new FileReader(inputFile);
      BufferedReader bufferedReader = new BufferedReader(reader);

      int intRead = -1;

      /** Will continue to loop until .read() = -1 which indicates the end of the file
          This also assumes the input is ASCII which can be converted to a char */
      while((intRead = bufferedReader.read()) != -1) {
          char charRead = (char) intRead;
          if(charRead != '\n') {
            inputString.append(charRead);
          }
      }

    } catch(FileNotFoundException fileNotFound) {
      System.out.println("Input fileName cannot be found, exception: " + fileNotFound);
    } catch(IOException ioException) {
      System.out.println("Error when loading the file, exception: " + ioException);
    }
    return inputString.toString();
  }
}
