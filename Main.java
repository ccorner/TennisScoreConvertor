/**
 * This program takes a commandline argument as the input file that for
 * each line represents a tennis match showing who won each point
 * either with an A or B for which player wins, this program takes that input
 * and creates an output showing the results of each match in the following format
 * [completed set] [score in current set] [score in current game]
 */
public class Main {

    public static void main(String[] args) {
        TennisScore tennisScore = new TennisScore();
        tennisScore.convertToScore(args[0]);
    }
}
