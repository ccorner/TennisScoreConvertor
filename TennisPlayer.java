public class TennisPlayer {

    private int setsWon;
    private int currentScore;
    private boolean hasSetFinished;

    public TennisPlayer() {
        setsWon = 0;
        currentScore = 0;
        hasSetFinished = false;
    }

    public void pointWon(TennisPlayer opposingPlayer) {

        if (currentScore < 30) {
            currentScore += 15;
        } else if (currentScore == 30) {
            currentScore += 10;
        } else if (currentScore == 40) {
            checkMatchPoint(opposingPlayer);
        } else {
            checkMatchPointAvantage(opposingPlayer);
        }

    }


    public int getSetsWon() {
        return setsWon;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public boolean getHasSetFinished() {
        return hasSetFinished;
    }

    public void resetScore() {
        currentScore = 0;
    }

    public void resetSets() {
        setsWon = 0;
    }

    public void setSetFinishedToFalse() {
        hasSetFinished = false;
    }

    private void checkMatchPoint(TennisPlayer opposingPlayer) {
        if (opposingPlayer.currentScore < 40) {
            setWon(opposingPlayer);
        } else if(opposingPlayer.currentScore == 50) {
            opposingPlayer.currentScore -= 10;
        }
        else {
            currentScore += 10;
        }
    }

    private void checkMatchPointAvantage(TennisPlayer opposingPlayer) {
        if (opposingPlayer.currentScore <= 40) {
            setWon(opposingPlayer);
        } else if (opposingPlayer.currentScore == 50) {
            opposingPlayer.currentScore -= 10;
        }
    }

    private void setWon(TennisPlayer opposingPlayer) {
        setsWon += 1;
        currentScore = 0;
        opposingPlayer.currentScore = 0;
        hasSetFinished = true;
    }

    public boolean gameWon(TennisPlayer opposingPlayer) {
        return (setsWon - opposingPlayer.setsWon) >= 2 && setsWon >= 6;
    }
}
