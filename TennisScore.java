import java.util.ArrayList;


public class TennisScore {

    private String inputFile;
    private ArrayList<String> completedGames;
    private TennisPlayer playerA;
    private TennisPlayer playerB;
    private TennisPlayer[] players;
    private int server;

    public TennisScore() {
        completedGames = new ArrayList();
        playerA = new TennisPlayer();
        playerB = new TennisPlayer();
        players = new TennisPlayer[2];
        players[0] = playerA;
        players[1] = playerB;
        server = 0;
    }


    public void convertToScore(String inputFileName) {
        LoadFile loadFile = new LoadFile();
        inputFile = loadFile.loadFile(inputFileName);

        for (char currentChar : inputFile.toCharArray()) {
            if (currentChar == '\r') {
                printScore();
            } else {
                if (currentChar == 'A') {
                    playerA.pointWon(playerB);
                } else {
                    playerB.pointWon(playerA);
                }

                if(playerA.getHasSetFinished() || playerB.getHasSetFinished()) {
                    server = 1 - server;
                    playerA.setSetFinishedToFalse();
                    playerB.setSetFinishedToFalse();
                }

                if (playerA.gameWon(playerB) || playerB.gameWon(playerA)) {
                    completedGames.add(players[server].getSetsWon() + "-" + players[getNonServer()].getSetsWon());
                    playerA.resetSets();
                    playerB.resetSets();
                }
            }
        }
        printScore();
    }

    private void printScore() {
         for (String completed : completedGames) {
            System.out.print(completed + " ");
        }

        System.out.print(players[server].getSetsWon() + "-" + players[getNonServer()].getSetsWon() + " ");

        if (playerA.getCurrentScore() == 0 && playerB.getCurrentScore() == 0) {
        } else {
            System.out.print(players[server].getCurrentScore() + "-" + players[getNonServer()].getCurrentScore());
        }
        resetStats();
        System.out.println();
    }

    private void resetStats() {
        playerA.resetScore();
        playerA.resetSets();
        playerB.resetScore();
        playerB.resetSets();
        completedGames.clear();
        server = 0;
    }
    // Alternates between 0 and 1 to pick the current server from the list of players
    private int getNonServer() {
        return 1 - server;
    }

}
